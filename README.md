# CSGO E-Ink Stats Display

Uses GSI (Game State Integration) [WIKI: CSGO GSI](https://developer.valvesoftware.com/wiki/Counter-Strike:_Global_Offensive_Game_State_Integration)to post data to a flask server running on a raspberry pi, or any device using a Papirus controller to run an e-ink display

Edit the GSI config with the rPi/eInk display's IP and place it `$gamefiles/csgo/cfg/gamestate_integration_eink.cfg`
 - Example: `cp gamestate_integration_eink.cfg ~/.steam/steam/steamapps/common/Counter-Strike\ Global\ Offensive/csgo/cfg/gamestate_integration_test.cfg`

## Requirements
- Flask
- Papirus Controller & E-Ink display

### Future Items

- Add Cool Icons
- Calculate other Teams Loss Bonus
- Guess if it would be a Buy or Eco round


