#!/usr/bin/env python3
from flask import Flask
from flask import request
from papirus import PapirusComposite

screen = PapirusComposite(False)
application = Flask(__name__)


def initialise():
    screen.AddText("Game: CS:GO", 0, 0, size=14, Id="Game")
    screen.AddText("Map: ", 0, 15, size=14, Id="Map")
    screen.AddText("T: 0 | CT: 0", 0, 30, Id="Score")
    screen.AddText("T Loss: 0 | CT: 0", 0, 50, Id="ScoreLoss")
    screen.AddText("H: 000 | A: 000 ", 0, 70, Id="Status")
    screen.AddText("Status Bar", 0, 90, Id="ExStats")
    screen.AddText("Action Bar", 0, 110, Id="Action")
    screen.WriteAll()


@application.route('/', methods=['POST'])
def index():
    if request.method == 'POST':
        data = request.get_json()
        if data["player"]["activity"] != "playing":
            return "200"
        my_id = str(data["provider"]["steamid"])
        csmap = str(data["map"]["name"])
        t_score = str(data["map"]["team_t"]["score"])
        t_consec_loss = str(data["map"]["team_t"]["consecutive_round_losses"])
        ct_score = str(data["map"]["team_ct"]["score"])
        ct_consec_loss = str(data["map"]["team_ct"]["consecutive_round_losses"])

        screen.UpdateText("Game", "G: "+my_id)
        screen.UpdateText("Map", csmap)
        screen.UpdateText("Score", "T: "+t_score+" | CT: "+ct_score)
        screen.UpdateText("ScoreLoss", "TL: {} | CTL: {}".format(t_consec_loss, ct_consec_loss))

        if data["player"]["steamid"] == my_id:
            # Im Alive
            state = data["player"]["state"]
            health = str(state["health"])
            armor = str(state["armor"])
            helmet = str(state["helmet"])

            if not helmet:
                screen.UpdateText("Action", "* * BUY HELMET * *")
            screen.UpdateText("Status", "H: "+health+", A: "+armor)

        kills = data["player"]["match_stats"]["kills"]
        assists = data["player"]["match_stats"]["assists"]
        deaths = data["player"]["match_stats"]["deaths"]
        score = data["player"]["match_stats"]["score"]

        screen.UpdateText("ExStatus", "KAD: {}|{}|{}|{}".format(kills, assists, deaths, score))
        screen.WriteAll(partialUpdate=True)
        return "200"


if __name__ == "__main__":
    initialise()
    application.run(host='0.0.0.0', port=8298)
